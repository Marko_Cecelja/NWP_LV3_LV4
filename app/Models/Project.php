<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['name', 'description', 'price', 'finished_jobs', 'start_date', 'end_date'];

    public function projectMembers() {
        return $this->hasMany(ProjectMember::class);
    }
}
