<?php

namespace App\Http\Controllers;

use App\Models\ProjectMember;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getUserView($project_id)
    {
        $users = User::orderBy('created_at', 'asc')->get();
        return view('users', [
            'users' => $users,
            'project_id' => $project_id
        ]);
    }

    public function addUser(Request $request)
    {
        $project_member = new ProjectMember();
        $project_member->user_id = $request->user_id;
        $project_member->project_id = $request->project_id;
        $project_member->role_id = 2;
        $project_member->save();
        return redirect('/projects');
    }
}
