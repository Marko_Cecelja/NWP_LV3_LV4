<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectMember;
use App\Repositories\ProjectRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{

    protected $projects;

    public function __construct(ProjectRepository $projects)
    {
        $this->middleware('auth');
        $this->projects = $projects;
    }

    public function index(Request $request)
    {
        return view('projects', [
            'projects' => $this->projects->forUser($request->user()),
        ]);
    }

    public function getNewProjectView()
    {
        return view('newproject');
    }

    public function getEditProjectView($project_id)
    {
        $project = Project::find($project_id);
        return view('editproject', [
            'project' => $project
        ]);
    }

    public function saveNewProject(Request $request)
    {
        $project = new Project;
        $project->name = $request->name;
        $project->description = $request->description;
        $project->price = $request->price;
        $project->finished_jobs = $request->finsihedjobs;
        $project->start_date = $request->startdate;
        $project->end_date = $request->enddate;
        $project->save();

        $project_member = new ProjectMember();
        $project_member->user_id = Auth::user()->getId();
        $project_member->project_id = $project->id;
        $project_member->role_id = 1;
        $project_member->save();
        return redirect('/projects');
    }

    public function editProject (Request $request, $project_id) {
        $project = Project::find($project_id);

        $isUserLeader = false;

        foreach ($project->projectMembers as $projectMember) {
            if ($projectMember->user_id == Auth::user()->getId() && $projectMember->role_id == 1) {
                $isUserLeader = true;
            }
        }

        if ($isUserLeader) {
            $project->name = $request->name;
            $project->description = $request->description;
            $project->price = $request->price;
            $project->start_date = $request->startdate;
            $project->end_date = $request->enddate;
        }
        $project->finished_jobs = $request->finishedjobs;
        $project->save();
        return redirect('/projects');
    }
}
