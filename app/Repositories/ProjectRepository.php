<?php

namespace App\Repositories;

use App\Models\Project;
use App\Models\User;

class ProjectRepository
{
    public function forUser(User $user)
    {
        return Project::leftJoin('project_members', 'projects.id', '=', 'project_members.project_id')
            ->where('user_id', $user->id)
            ->orderBy('created_at', 'asc')
            ->get();
    }
}
