@extends('layouts.app')
@section('content')
    <!DOCTYPE html>
<html lang="en">
<head>
    <title>All projects</title>
    <style>
        .table {
            margin-top: 10%;
        }
    </style>
</head>
<body>
<div class="container table-responsive">
    @if(count($projects) > 0)
        <table class="table table-striped table-hover">
            <thead class="thead-dark">
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Finished jobs</th>
            <th>Start date</th>
            <th>End date</th>
            <th></th>
            <th></th>
            </thead>
            <tbody>
            @foreach ($projects as $project)
                <tr>
                    <td>
                        <div>{{$project->id}}</div>
                    </td>
                    <td>
                        <div>{{$project->name}}</div>
                    </td>
                    <td>
                        <div>{{$project->description}}</div>
                    </td>
                    <td>
                        <div>{{$project->price}}</div>
                    </td>
                    <td>
                        <div>{{$project->finished_jobs}}</div>
                    </td>
                    <td>
                        <div>{{$project->start_date}}</div>
                    </td>
                    <td>
                        <div>{{$project->end_date}}</div>
                    </td>
                    <td><a class="link" href="{{ route('editproject', $project->id) }}">Edit</a></td>
                    @php($isUserLeader = false)
                    @foreach($project->projectMembers as $projectMember)
                        @if($projectMember->user_id == Auth::user()->getId() && $projectMember->role_id == 1)
                            @php($isUserLeader = true)
                        @endif
                    @endforeach
                    @if($isUserLeader)
                        <td><a class="link" href="{{ route('users', $project->id) }}">Add user</a></td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        No projects.
    @endif
</div>
</body>
</html>
@endsection
