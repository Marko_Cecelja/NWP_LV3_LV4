@extends('layouts.app')

@section('content')
    <!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit project</title>
</head>
<body>
<form class="container col-md-4 col-md-offset-4" method="POST" action="{{ route('saveproject', $project->id) }}">
    @csrf
    @php($isUserLeader = false)
    @foreach($project->projectMembers as $projectMember)
        @if($projectMember->user_id == Auth::user()->getId() && $projectMember->role_id == 1)
            @php($isUserLeader = true)
        @endif
    @endforeach
    <input type="hidden" name="_method" value="PUT">
    @if($isUserLeader)
        <div class="form-group">
            <label for="name">Name: </label>
            <input class="form-control" type="text" name="name" id="name" value="{{$project->name}}"
                   required>
        </div>
        <div class="form-group">
            <label for="description">Description: </label>
            <textarea class="form-control" rows=5 name="description" id="description"
                      required>{{$project->description}}</textarea>
        </div>
        <div class="form-group">
            <label for="price">Price: </label>
            <input class="form-control" type="number" name="price" id="price" value="{{$project->price}}"
                   required>
        </div>
        <div class="form-group">
            @endif
            <label for="finishedjobs">Jobs done: </label>
            <textarea class="form-control" rows=5 name="finishedjobs" id="finishedjobs"
                      required> {{$project->finished_jobs}}</textarea>
        </div>
        @if($isUserLeader)
            <div class="form-group">
                <label for="startdate">Start date: </label>
                <input class="form-control" type="date" name="startdate" id="Start date"
                       value="{{$project->start_date}}" required>
            </div>
            <div class="form-group">
                <label for="enddate">End date: </label>
                <input class="form-control" type="date" name="enddate" id="enddate"
                       value="{{$project->end_date}}" required>
            </div>
        @endif
        <button type="submit" class="btn btn-primary">Submit</button>
</form>
</body>
</html>
@endsection
