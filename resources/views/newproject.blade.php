@extends('layouts.app')

@section('content')
    <!DOCTYPE html>
<html lang="en">
<head>
    <title>New project</title>
</head>
<body>
<form class="container col-md-4 col-md-offset-4" method="POST" action="{{ route('project') }}">
    @csrf
    <div class="form-group">
        <label for="name">Name: </label>
        <input class="form-control" type="text" name="name" id="name" required>
    </div>
    <div class="form-group">
        <label for="description">Description: </label>
        <textarea class="form-control" rows=5 name="description" id="description" required></textarea>
    </div>
    <div class="form-group">
        <label for="price">Price: </label>
        <input class="form-control" type="number" name="price" id="price" required>
    </div>
    <div class="form-group">
        <label for="finsihedjobs">Finished jobs: </label>
        <textarea class="form-control" rows=5 name="finsihedjobs" id="finsihedjobs" required></textarea>
    </div>
    <div class="form-group">
        <label for="startdate">Start date: </label>
        <input class="form-control" type="date" name="startdate" id="Start date" required>
    </div>
    <div class="form-group">
        <label for="enddate">End date: </label>
        <input class="form-control" type="date" name="enddate" id="enddate" required>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</body>
</html>
@endsection
