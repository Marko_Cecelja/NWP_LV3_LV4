<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'projects'], function () {
    Route::get('/', 'App\Http\Controllers\ProjectController@index')->name('projects');
    Route::get('/new', 'App\Http\Controllers\ProjectController@getNewProjectView')->name('newproject');
    Route::get('/edit/{project_id?}', 'App\Http\Controllers\ProjectController@getEditProjectView')->name('editproject');
    Route::post('/', 'App\Http\Controllers\ProjectController@saveNewProject')->name('project');
    Route::put('/save/{project_id?}', 'App\Http\Controllers\ProjectController@editProject')->name('saveproject');
});

Route::group(['prefix' => 'users'], function () {
    Route::get('/{project_id?}', 'App\Http\Controllers\UserController@getUserView')->name('users');
    Route::post('/add', 'App\Http\Controllers\UserController@addUser')->name('adduser');
});

